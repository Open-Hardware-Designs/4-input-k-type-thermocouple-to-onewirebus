EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_Modular-Connectors-Jacks:54602-908LF J2
U 1 1 5FF7E8C3
P 4725 3825
F 0 "J2" H 5280 3866 50  0000 L CNN
F 1 "54602-908LF" H 5280 3775 50  0000 L CNN
F 2 "digikey-footprints:Ethernet_Jack_54602-908LF" H 4925 4025 50  0001 L CNN
F 3 "" H 4925 4125 60  0001 L CNN
F 4 "609-1046-ND" H 4925 4225 60  0001 L CNN "Digi-Key_PN"
F 5 "54602-908LF" H 4925 4325 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 4925 4425 60  0001 L CNN "Category"
F 7 "Modular Connectors - Jacks" H 4925 4525 60  0001 L CNN "Family"
F 8 "" H 4925 4625 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/amphenol-icc-fci/54602-908LF/609-1046-ND/1001360" H 4925 4725 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN MOD JACK 8P8C R/A UNSHLD" H 4925 4825 60  0001 L CNN "Description"
F 11 "Amphenol ICC (FCI)" H 4925 4925 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4925 5025 60  0001 L CNN "Status"
	1    4725 3825
	1    0    0    -1  
$EndComp
$Comp
L dk_Modular-Connectors-Jacks:54602-908LF J1
U 1 1 5FF7F2AD
P 3225 3825
F 0 "J1" H 3780 3866 50  0000 L CNN
F 1 "54602-908LF" H 3780 3775 50  0000 L CNN
F 2 "digikey-footprints:Ethernet_Jack_54602-908LF" H 3425 4025 50  0001 L CNN
F 3 "" H 3425 4125 60  0001 L CNN
F 4 "609-1046-ND" H 3425 4225 60  0001 L CNN "Digi-Key_PN"
F 5 "54602-908LF" H 3425 4325 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 3425 4425 60  0001 L CNN "Category"
F 7 "Modular Connectors - Jacks" H 3425 4525 60  0001 L CNN "Family"
F 8 "" H 3425 4625 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/amphenol-icc-fci/54602-908LF/609-1046-ND/1001360" H 3425 4725 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN MOD JACK 8P8C R/A UNSHLD" H 3425 4825 60  0001 L CNN "Description"
F 11 "Amphenol ICC (FCI)" H 3425 4925 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3425 5025 60  0001 L CNN "Status"
	1    3225 3825
	1    0    0    -1  
$EndComp
Text Notes 4450 2575 0    50   ~ 0
1 – RX (+) –Receive Data (+) —————- WHITE/ORANGE — none\n2 – RX (-) – Receive Data (-) —————–ORANGE ————– YELLOW\n3 – TX (+) – Transmit Data (+) —————WHITE/GREEN ——none\n4 – DC (+) –Power-over-Eithernet (+) —– BLUE ——————  RED\n5 – DC (+) –Power-over-Eithernet (+) —   WHITE/BLUE———-none\n6 – TX (-) –  Transmit Data (-) ————— GREEN —————  none\n7–  DC (-) – Power-over-Eithernet (-) ——WHITE/BROWN ——none\n8 – DC(-) –  Power-over-Eithernet (-) ——BROWN —————  BLACK
$EndSCHEMATC
