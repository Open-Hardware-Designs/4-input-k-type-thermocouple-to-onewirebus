EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_POT RV1
U 1 1 5FFA6302
P 3875 2250
AR Path="/5FFA4D5C/5FFA6302" Ref="RV1"  Part="1" 
AR Path="/5FFA89DE/5FFA6302" Ref="RV2"  Part="1" 
F 0 "RV1" V 3668 2250 50  0000 C CNN
F 1 "R_POT" V 3759 2250 50  0000 C CNN
F 2 "" H 3875 2250 50  0001 C CNN
F 3 "~" H 3875 2250 50  0001 C CNN
	1    3875 2250
	0    1    1    0   
$EndComp
Text HLabel 3225 2025 0    50   Input ~ 0
SDD
Wire Wire Line
	3225 2025 3525 2025
Wire Wire Line
	3525 2025 3525 2250
Wire Wire Line
	3525 2250 3725 2250
Text HLabel 3025 2600 0    50   Input ~ 0
FSS
Text HLabel 2925 3025 0    50   Input ~ 0
AGRR
$EndSCHEMATC
